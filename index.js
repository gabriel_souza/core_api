
//TODO: organizar melhor

const initializeApi = async () => {
  const env = require('./libs/environmentalVariables');
  const validedEnvs = env.hasAllMandatoryEnvironmentalVariables();
  
  if (validedEnvs) {
    const logger = require('./libs/logger');
    const uuidV5 = require('uuid/v5');
    const uuidV1 = require('uuid/v1');

    const fastify = require('fastify')({
      logger: logger,
      genReqId: function(req) {
        
        return `${uuidV5(req.method+req.url, uuidV1())}`;
      },
    });
    const fastify_helmet = require('fastify-helmet');
    const routers = require('./libs/routers');
    const process = require('process');
    const path = require('path');
    const check =  require('./libs/healthCheck');
    const pathExecution = process.argv[1];

    logger.debug('[CORE] Variaveis obrigatorias carregadas');
    fastify.register(fastify_helmet);
    check.define(fastify);
    await routers.attach(fastify, path.normalize(pathExecution + '/..'), logger);
    const port = env.getEnvironmental('EXPOSE_PORT', 3000)


    fastify.listen(port, (err) => {
      if(err){
        logger.debug(`[CORE] Erro ao subir api na porta ${port}`);
      } else {
        logger.debug(`[CORE] Api up na porta ${port} `);
      }
    });
  } else {
    console.log('[CORE] nao a todas as variaves obrigatorias');
  }
};

initializeApi();
