/* eslint-disable */
const { expect } = require('chai');
const sinon = require('sinon').createSandbox();


const mock_require = require('mock-require');
const fastify = require('fastify')();

describe('test Up router', function() {
  after(function () {
    mock_require.stopAll();
    sinon.restore();
  });
  
  describe('teste up rota sem configuracao', function () {
  let router = undefined;
  afterEach(function() {
    mock_require.stopAll();
    sinon.restore();
  });
  beforeEach(function() {
    mock_require('../../../libs/routers/loader', () => ([{
      method: 'get',
      url: '/lua/nova',
      valid: true,
      controller: () => {},
      }]) 
    );

     mock_require('../../../libs/cache', function () {
       return {
         get: () => {},
         set: () => {}
      }
     });

    sinon.spy(fastify, 'route');
    
    router = require('../../../libs/routers/index.js');
  });
  
    it('services registrar rota sem config e sem validate', function() {
      router.attach(fastify, '');
      const routerRegisted = fastify.route.getCall(0);
      
      expect(routerRegisted.args).to.lengthOf(1);
      expect(routerRegisted.args[0].method).to.equal('GET');
      expect(routerRegisted.args[0].url).to.equal('/lua/nova');
      expect(routerRegisted.args[0].handler).to.be.a('function');
    });
  });
  
  
  describe('teste up rota com configuracao', function () {
    let stubGetCache = sinon.stub();
    let stubSetCache = sinon.stub();
    beforeEach(function () {
      mock_require('../../../libs/routers/loader', () => ([{
        method: 'get',
        url: '/lua/nova',
        valid: true,
        controller: () => {},
        config: {
          cache: {
            active:true,
            expiredIn: '3H'
          }
        }
      }]));
      const cache = function () {
        return {
          get: stubGetCache.resolves('memoria de peixe'),
          set: stubSetCache.resolves()
        }
      };
      
      mock_require('../../../libs/cache', cache);

      sinon.spy(fastify, 'route');

      router = require('../../../libs/routers/index.js');
    });





    it('services registrar rota com config de cache', function() {
      router.attach(fastify, '');
      const routerRegisted = fastify.route.getCall(0);
      expect(routerRegisted.args).to.lengthOf(1);
      expect(routerRegisted.args[0].method).to.equal('GET');
      expect(routerRegisted.args[0].url).to.equal('/lua/nova');
      expect(routerRegisted.args[0].handler).to.be.a('function');
     });
  });
});
