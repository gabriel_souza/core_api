/* eslint-disable */
const { expect } = require('chai');

describe('test Load router', function() {
  let loadRouter = undefined;

  beforeEach(function() {
    loadRouter = require('../../../libs/routers/loader.js');
  });

  it('services sem rotas', function() {
    const fullPath =
      '/home/gabriel/Documentos/workspace/t/node_modules/core_api/test/testAssets/loaderRouters/semServices';
    const result = loadRouter(fullPath);
    expect(result).to.have.lengthOf(0);
  });
  it('services sem rotas validas', function() {
    const fullPath =
      '/home/gabriel/Documentos/workspace/t/node_modules/core_api/test/testAssets/loaderRouters/comServicesNaoValidas';
    const result = loadRouter(fullPath);
    expect(result).to.have.lengthOf(0);
  });
  it('services com uma rota valida', function() {
    const fullPath =
      '/home/gabriel/Documentos/workspace/t/node_modules/core_api/test/testAssets/loaderRouters/comUmaServiceValida';
    const result = loadRouter(fullPath);

    expect(result).to.have.lengthOf(1);
    expect(result[0].method).to.have.string('get');
    expect(result[0].url).to.have.string('/user');
    expect(result[0].valid).to.equal(true);
    expect(result[0].controller()).to.have.string('oi sou get');
  });
  it('services com duas rota valida', function() {
    const fullPath =
      '/home/gabriel/Documentos/workspace/t/node_modules/core_api/test/testAssets/loaderRouters/comDuasServiceValida';
    const result = loadRouter(fullPath);

    expect(result).to.have.lengthOf(2);
    expect(result[1].method).to.have.string('get');
    expect(result[1].url).to.have.string('/user/:id');
    expect(result[1].valid).to.equal(true);
    expect(result[1].controller()).to.have.string('oi sou get');
  });
});
