const redis = require('redis');
const process = require('process');
const bluebird = require('bluebird');
const redis_promise = bluebird.promisifyAll(redis);
const _erGetUnitInConfigTimeCache = /.{1}$/;
const _erGetNumberInConfigTimeCache = /^\d+/;

const getTime = (time) => {
  return _erGetNumberInConfigTimeCache.exec(time)[0];
};
const convertMinutesInSeconds = (minutes) => {
  return minutes * 60;
};
const convertHoursInSeconds = (hours) => {
  return hours * 3600;
};

const convertTimeInSeconds = (configTime) => {
  let seconds = 60;

  switch (configTime && configTime.match(_erGetUnitInConfigTimeCache)[0]) {
    case 'M': {
      const minutes = getTime(configTime);

      seconds = convertMinutesInSeconds(minutes);
      break;
    }
    case 'H': {
      const hours = getTime(configTime);

      seconds = convertHoursInSeconds(hours);
      break;
    }
    case 'S': {
      seconds = getTime(configTime);
      break;
    }
  }

  return seconds;
};

const getConnRedis = (logger) => {
  return new Promise((resolved) => {
    const conn = redis_promise.createClient(process.env['HOST_REDIS']);

    conn.on('connect', () => {
      //log
      logger.debug('[CACHE] Conectado ao redis ');
      resolved(conn);
    });

    conn.on('error', (err) => {
      //log
      logger.debug(`[CACHE] Erro ao connectar ao redis ${err}`);
      resolved();
    });
    conn.on('reconnecting', () => {
      logger.debug(`[CACHE] Reconectando ao redis `);
    });
  });
};

const cache = async (logger) => {
  const connRedis = await getConnRedis(logger);

  return {
    ready: !connRedis || connRedis.connected,
    get: (key) => {
      debugger
      logger.debug('[CACHE] get valor no redis');
      return connRedis.getAsync(key);
    },
    set: ({ key, value, expiredIn }) => {
      debugger
      logger.debug('[CACHE] set valor no redis');
      const expiredInXSeconds = convertTimeInSeconds(expiredIn);
      return connRedis.setAsync(key, value, 'EX', expiredInXSeconds);
    },
  };
};

module.exports = cache;
