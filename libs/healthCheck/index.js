module.exports.define = (fastify) => {
  fastify.get('/healthCheck', async () => {
    return 'ok';
  });
};
