const process = require('process');
const mandatoryEnvironmentalVariablesList = require('./mandatoryEnvironmentalVariables.json');

 module.exports = {
    hasAllMandatoryEnvironmentalVariables:() => {
      const envVariableLoadInProcess = process.env;
      
      return mandatoryEnvironmentalVariablesList
        .filter((env) => {
          return envVariableLoadInProcess[env];
        })
        .length == mandatoryEnvironmentalVariablesList.length
    },
    getEnvironmental: (key, alternativeValue=undefined) => {
      const envVariableLoad = process.env[key];
      
      if(envVariableLoad) {
        return envVariableLoad;
      } else {
        return alternativeValue;
      }
    }
 };
