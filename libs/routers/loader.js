const path = require('path');
const fs = require('fs');

const getPathStartedProcess = (fullPath) => {
  return path.normalize(fullPath.concat('/services'));
};

const loadFoldersService = (path) => {
  try {
    const pathServices = fs.readdirSync(path);

    if (pathServices && pathServices.length) {
      //log carregando x servicos
      return pathServices.filter((i) => !/\..+$/.test(i));
    } else {
      // log nenhum service encontrado
      return [];
    }
  } catch (error) {
    return [];
  }
};

const getMethodsServiceInFolder = (pathService) => {
  const folders = fs.readdirSync(pathService);

  return folders
    .toString('')
    .match(
      /(?:(GET)|(HEAD)|(POST)|(PUT)|(DELETE)|(CONNECT)|(OPTIONS)|(TRACE)|(PATCH))/gi
    );
};

const validFilesService = (pathService) => {
  const files = fs.readdirSync(pathService);
  //default config index
  //opcional validate
  return files.indexOf('index.js') > -1;
};

const hasFileValidate = (pathService) => {
  const files = fs.readdirSync(pathService);
  return files.indexOf('validate.js') > -1;
};

const hasEspecialConfig = (pathService) => {
  const files = fs.readdirSync(pathService);
  return files.indexOf('config.json') > -1;
};

/**
 * doc
 * nome da pasta 
 * nome_usuario
 * subistituido por 
 * nome/usuario
 * 
 * nome_usuario_{id}
 * nome/usuario/:id
 * **/

const parseNameFolderInRouterPath = (nameFolder) =>
  '/'.concat(
    nameFolder
      .replace(/_/g, '/')
      .replace(/(-[A-Z]+)/g, (_, match) =>
        match.tolowerCase().replace(/-/, ':')
      )
  );

const mapMethodsUrlPathFolderService = (pathServices) => {
  return (pathInFolderService) => {
    const fullPathForService = pathServices.concat('/', pathInFolderService);
    const methods = getMethodsServiceInFolder(fullPathForService);

    return {
      methods: methods || [],
      url: parseNameFolderInRouterPath(pathInFolderService),
      pathFolderService: fullPathForService,
    };
  };
};

const reduceOrganizeServiceByMethod = (acc, current) => {
  const listByMethod = current.methods.map((method) => ({
    method: method,
    url: current.url,
    pathFolderService: current.pathFolderService,
  }));

  acc = acc.concat(listByMethod);

  return acc;
};

const mapperConfigRouter = (
  { method, url },
  isValidMethodService,
  pathFolderServiceWithMethod
) => {
  const hasValidate = hasFileValidate(pathFolderServiceWithMethod);
  const hasConfig = hasEspecialConfig(pathFolderServiceWithMethod);

  return {
    method,
    url,
    valid: isValidMethodService,
    controller: isValidMethodService
      ? require(pathFolderServiceWithMethod.concat('/index.js'))
      : undefined,
    config: hasConfig
      ? require(pathFolderServiceWithMethod.concat('/config.json'))
      : undefined,
    validate: hasValidate
      ? require(pathFolderServiceWithMethod.concat('/validate.js'))
      : undefined,
  };
};

const loadRoutersByConfigs = (config) => {
  const pathFolderServiceWithMethod = config.pathFolderService.concat(
    '/',
    config.method
  );

  const isValidMethodService = validFilesService(pathFolderServiceWithMethod);

  return mapperConfigRouter(
    config,
    isValidMethodService,
    pathFolderServiceWithMethod
  );
};

const getRouters = (fullPath) => {
  const pathServices = getPathStartedProcess(fullPath);
  const pathsInFolderService = loadFoldersService(pathServices);

  if (!pathsInFolderService.length) {
    return [];
  }

  const services = pathsInFolderService
    .map(mapMethodsUrlPathFolderService(pathServices))
    .reduce(reduceOrganizeServiceByMethod, [])
    .map(loadRoutersByConfigs);

  if (services && services.length > 0) {
    return services;
  } else {
    //log
    return [];
  }
};

module.exports = getRouters;
