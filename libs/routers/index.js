const loaderRouters = require('./loader');
const cache = require('../cache');

const defineHooks = (fastify, configs, cache) => {
  fastify.addHook('onRoute', function(routerOptions) {
    routerOptions.onSend = defineHookOnSendResponse.bind({
      configs,
      cache,
      path: routerOptions.path,
    });

    routerOptions.onRequest = defineHookOnRequest.bind({
      configs,
      cache,
      path: routerOptions.path,
    });
  });
};

const getCurrentConfigInHashList = ({ configs, path }) => {
  const keyForConfig = 'GET' + path;
  return configs[keyForConfig];
};

const defineHookOnSendResponse = async function (request, reply, payload){
  const configService = getCurrentConfigInHashList(this);
  const methodRequest = request.raw.method.toUpperCase();
  debugger
  if (
      configService && 
      configService.cache && 
      configService.cache.active && 
      methodRequest == 'GET' && 
      !request.responseCached && 
      (
        reply.res.statusCode && reply.res.statusCode[0] == 2
      )

    ) {
    request.log.info(`Escrevendo cache expira em ${configService.cache.expiredIn}`);
    const keyForCache = methodRequest + request.raw.url;
    
    await this.cache.set({
      key: keyForCache,
      value: payload,
      expiredIn: configService.cache.expiredIn
    });
  }
  return payload;
};

const defineHookOnRequest = async function (request, reply){
  
  const configService = getCurrentConfigInHashList(this);
  const methodRequest = request.raw.method.toUpperCase();
  if (
      configService &&
      configService.cache &&
      configService.cache.active &&
      methodRequest == 'GET'
    ) {
    
    const keyForCache = methodRequest + request.raw.url;
    debugger
    
    const response = await this.cache.get(keyForCache);
    if (response) {
      request.responseCached = true;
      request.log.info('Recuperado cache');
      reply.send(
        JSON.parse(response)
      );
    } else {
      return;
    }
  } else {
    return;
  }
};

const convertRouterConfigArrayInHashList = (list) => {
  return list.reduce((acc, current) => {
    
    
    if (current.config) {
      acc[current.method.toUpperCase() + current.url] = current.config;
    }

    return acc;
  }, {});
};

const attach = async (fastify, path, logger) => {
  logger.debug('[CORE] Carregando rotas');
  const routers = loaderRouters(path);
  const hasListConfig = convertRouterConfigArrayInHashList(routers);
  defineHooks(fastify, hasListConfig, await cache(logger));
  
  if (routers.length){
    routers.forEach((router) => {
      logger.debug(`[CORE] Registrando rota ${router.method}:${router.url}`);
      fastify.route({
        method: router.method.toUpperCase(),
        url: router.url,
        handler: function(request, reply) {
          const req = {
            query: request.query,
            headers: request.headers,
            params: request.params,
            body: request.body,
            log: request.log,
          };
        
          try{
            router.controller(req, reply);
          } catch(e){
            debugger
            request.log.fatal(e);
            
            reply
              .code(500)
              .send({
                error: 'Erro no processamento',
                code: 500 ,
                message: 'Erro no processamento',
                statusCode: 500,
                reqId: request.raw.id
              })
          }
        },
      });
    });
  } else {
    logger.debug(`[CORE] Nenhuma rota carregada`);
  }
};

module.exports.attach = attach;
