const pinoMultiStream = require('pino-multi-stream');
const pinoElastc = require('pino-elasticsearch');
const env = require('../environmentalVariables');

const configLoggerElastic = pinoElastc({
  index: env.getEnvironmental('NAME_INDEX_ELASTICSEARCH', 'core_api') + '-logs-%{DATE}',
  type: 'log',
  consistency: 'quorum',
  node: env.getEnvironmental('HOST_ELASTICSEARCH'),
  'bulk-size': 200,
  'es-version': 6,
  ecs: true,
});

const streams = {
  streams: [
    {
      level: 'trace',
      stream: pinoMultiStream.prettyStream(),
    },
    {
      level: 'info',
      stream: configLoggerElastic,
    },
  ],
};

const logger = pinoMultiStream(streams);

module.exports = logger;
