const fs = require('fs');
const pino = require('pino-multi-stream');
const child = require('child_process');


const log = pino(
 {
  streams: [{
      level: 'trace',
      stream: pino.prettyStream(),
    }
  ],
});

const writeFile = (destination, from, nameFile) => {
  const content = fs.readFileSync(from);
  console.log(content)
  console.log(destination)
  console.log(nameFile)
  return fs.writeFileSync(destination.concat('/',nameFile), content);
}

const writePackageJsonInFolder = ( path ) => 
  writeFile(path, __dirname.concat('/assets/package.txt'), 'package.json');

const writeStarterInFolder = ( path ) => 
  writeFile(path, __dirname.concat('/assets/init.txt'), 'start.js');

const writeIndexInFolderService = (path) =>
  writeFile(path, __dirname.concat('/assets/indexRouter.txt'), 'index.js');

const createFolder = (path) => {
  try {
    return fs.mkdirSync(path);
  } catch (error) {
    return error;
  }
}




const buildFolder = (path,nameFolder) => {
  const resultCreateFolderInitial = createFolder(path.concat('/', nameFolder));
  debugger
  if (!resultCreateFolderInitial) {
    const resultCreateFolderServices = createFolder(path.concat('/', nameFolder, '/services'));

    if (!resultCreateFolderServices) {
      const pathFolderProject = path.concat('/', nameFolder);
      log.info('Escrevendo package.json');
      writePackageJsonInFolder(pathFolderProject);
      log.info('Escrevendo starter');
      writeStarterInFolder(pathFolderProject);
      log.info('Instalando dependencias');
      child.exec('npm install',{
        cwd: pathFolderProject
      }).on('message',(e)=>{
        log.info(e);
      });

    } else {
      log.warn(`Erro ao criar pasta /services ${resultCreateFolderServices}`);
    }

  } else {
    //colocar log aqui
    log.fatal(`Erro ao criar pasta ${nameFolder} ${resultCreateFolderInitial}`);
  }
}


const validProject = (path) => {
  try {
    return fs.accessSync(path);
  } catch (error) {
    return error;
  }
}

const buildRouter =  (path, method, pathRouter) => {
  const pathWithServicePath = path.concat('/services');
 
  const isValidFolderProject = validProject(pathWithServicePath);
  
  if (!isValidFolderProject) {
    const pathWithRouter = pathWithServicePath.concat('/', pathRouter);
    const result = createFolder(pathWithRouter);
    
    if (!result) {
      const pathWithMethod = pathWithRouter.concat('/', method);
      const result = createFolder(pathWithMethod);
      if (!result){
        writeIndexInFolderService(pathWithMethod);
      } else {
        console.l
       log.warn(`Erro ao criar pasta de method ${result}`);
      }
    } else {
       log.warn(`Erro ao criar pasta da rota ${result}`);
    }
  } else {
    log.warn('projeto invalido, nao foi possivel localizar a pasta services');
  }

}



module.exports = (path, option, paramsOption) => {
  switch(option){
    case('iniciar') :{
      buildFolder(path, paramsOption[0]);
      break;
    };
    case('criar_rota') :{
      buildRouter(path, paramsOption[0], paramsOption[1])
    }
  }
}


